# Error handled

Are you annoyed that code like this 

```
with open(file) as f:
    print(f.read())
```

can cause many different exceptions that will crash your program? And are you also annoyed that the usually prescribed monad to fix this problem completely get's rid of all information, *why* the call failed?

```
def read_file(file: str) -> str | None:
    try:
        with open(file) as f:
            return f.read()
    except:
        return None
```

Enter `error_handled`. Together with Python 3.10's new pattern matching syntax safe programming is a blast. Are you interested in handling all the different error cases differently?

```
match (f := open_file("fileame")):
    case str(): print(f)
    case FileNotFoundError(): print("File not found")
    case PermissionError(): print("Permission")
    case _: print("Unknown error")
```

Are you not interested in the specific errors? That's still possible!

```
match (f := open_file("fileame")):
    case str(): print(f)
    case _: print("Failed reading file")
```