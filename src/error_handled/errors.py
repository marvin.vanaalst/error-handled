from __future__ import annotations

from typing import Optional

FileNotFound = FileNotFoundError
InsufficientPermissions = PermissionError


class Error:
    parent: Optional[Error] = None


class AssertionNotMatched(Error):
    ...


class EndOfFile(Error):
    ...


class CouldNotImport(Error):
    ...


class ModuleNotFound(Error):
    ...


class IndexOutOfRange(Error):
    ...


class KeyNotFound(Error):
    ...


class KeyboardInterrupt(Error):
    ...


class Overflow(Error):
    ...


class OutOfMemory(Error):
    ...


class NameNotFound(Error):
    ...


class NotImplemented(Error):
    ...


class OsOperationFailed(Error):
    ...


class DivisionByZero(Error):
    ...


class RecursionDepthExceeded(Error):
    ...


class ReferenceNotExistent(Error):
    ...


class IterationStopped(Error):
    ...


class IncorrectSyntax(Error):
    ...


class IncorrectIndentation(Error):
    ...


class InconsistentTabAndSpaceUse(Error):
    ...


class SystemExit(Error):
    ...


class InappropriateType(Error):
    ...


class ReferencedUnboundLocal(Error):
    ...


class UnicodeConversionFailure(Error):
    ...


class InappropriateValue(Error):
    ...
