from __future__ import annotations

from contextlib import contextmanager
from os import PathLike
from typing import Generator, TextIO, Union

StrOrBytesPath = Union[str, bytes, PathLike[str], PathLike[bytes]]

_OpenFile = Union[StrOrBytesPath, int]
_FileResult = Union[TextIO, FileNotFoundError, PermissionError]


@contextmanager
def safe_open(filename: _OpenFile) -> Generator[_FileResult, None, None]:
    try:
        f = open(filename)
        try:
            yield f
        finally:
            f.close()
    except FileNotFoundError:
        yield FileNotFoundError()
    except PermissionError:
        yield PermissionError()


if __name__ == "__main__":
    with safe_open("filename") as f:
        match f:
            case str():
                print(f)
            case FileNotFoundError():
                print("File not found")
            case PermissionError():
                print("Permission")
            case _:
                print(f"Unknown error {f}")

    with safe_open("filename") as f:
        match f:
            case str():
                print(f)
            case _:
                print("Failed reading file")
